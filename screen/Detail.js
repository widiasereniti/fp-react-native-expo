import React from 'react'
import { 
    View,
    Text,
    Image,
    ScrollView,
    TextInput,
    StyleSheet,
    TouchableOpacity
} from 'react-native'

export function Detail({navigation}) {
    return (
        <ScrollView>
            <View style={styles.container} >
                <TouchableOpacity onPress={() => navigation.goBack()}>
                    <Image style={styles.backButton} source={require('../assets/images/left-arrow.png')} />
                </TouchableOpacity>
                <View style={styles.containerFotoProduct}>
                    <TextInput style={styles.title}>Tiare Handwash</TextInput>
                    <Image style={styles.fotoProduct} source={require('../assets/images/Rectangle-4.png')}/>
                    <Text style={styles.priceProduct} >$12.23</Text>
                </View>

                <View style={styles.containerDescr}>
                    <Text style={styles.description}>
                        Tiare Handwash is blas ahsasdahsdsbdjhasd
                        dsahdjsdsdasd
                        sdhasjdasjdsad
                    </Text>
                </View>

                <Text style={styles.headerFlash} >Another Product</Text>
                <View style={styles.containerCategory}>
                    <View style={styles.Category}>
                        <Image style={styles.flash} source={require('../assets/images/Rectangle-1.png')}/>
                        <Text style={styles.product} >Nike Man Shoes</Text>
                        <Text style={styles.price} >$12.23</Text>
                    </View>
                    <View style={styles.Category}>
                        <Image style={styles.flash} source={require('../assets/images/Rectangle-3.png')}/>
                        <Text style={styles.product} >JBL Speaker</Text>
                        <Text style={styles.price} >$12.23</Text>
                    </View>
                    <View style={styles.Category}>
                        <Image style={styles.flash} source={require('../assets/images/Rectangle-2.png')}/>
                        <Text style={styles.product} >Google Home</Text>
                        <Text style={styles.price} >$80.00</Text>
                    </View>
                </View>
            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'column',
        justifyContent: 'center',
        backgroundColor: 'white',
    },

    containerFotoProduct: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },

    btnProfile: {
        marginLeft: 7,
        width: 40,
        height:40,
        borderRadius: 50 / 2,
    },

    priceProduct: {
        fontWeight: 'bold',
        fontSize: 20
    },

    title: {
        marginTop: 30,
        fontSize: 23,
        fontWeight: 'bold',
        color: '#003366',
        fontFamily: 'Roboto',
    },

    description: {
        marginTop: 30,
        marginHorizontal: 25,
        fontSize: 16,
        color: 'black',
        fontFamily: 'Roboto',
    },

    fotoProduct: {
        width: 350,
        height: 400,
        marginVertical: '5%',
        borderRadius: 7,
    },

    backButton: {
        width: 20,
        height: 20,
        marginTop: 50,
        marginLeft: 30,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },

    containerDescr: {
        marginVertical: 10,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },

    containerCategory: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: '5%',
    },

    Category: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },

    iconCategory: {
        maxWidth: 65,
        maxHeight: 65
    },

    iconCategoryHome: {
        maxWidth: 46,
        maxHeight: 46,
        marginBottom: 12
    },

    textInput: {
        fontSize: 12,
    },

    headerFlash: {
        marginVertical: '5%',
        marginHorizontal: '5%',
        fontSize: 20,
        fontWeight: 'bold'
    },

    flash: {
        width: 100,
        height: 100,
        borderRadius: 5,
        marginVertical: 5
    },

    price: {
        fontWeight: 'bold',
    },

    //Product
    containerProduct: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: '5%',
    },

    headerNewProduct: {
        marginVertical: '5%',
        marginHorizontal: '5%',
        fontSize: 20,
        fontWeight: 'bold'
    },

    imageProduct: {
        width: 160,
        height: 160,
        borderRadius: 5,
        marginVertical: 5
    },

    price: {
        fontWeight: 'bold',
        marginBottom: 20
    }
});
