import React, {useEffect} from 'react'
import { Image, StyleSheet, View } from 'react-native'

const Splash = ({navigation}) => {
    useEffect(()=>{
        setTimeout(()=>{
          navigation.replace('Login')
        }, 3000)
      },[])

    return (
        <View style={styles.container}>
            <View style={styles.containerWrapper}>
                <Image style={{width: 223, height: 133}} source={require('./../assets/images/logo.png')}/>
            </View>
        </View>
    )
}

export default Splash

const styles = StyleSheet.create({
    container: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
        height: '100%'
    },

    containerWrapper:{
        width: 300,
        height:300,
        borderRadius: 300 / 2,
        backgroundColor:'#E5E5E5',
        justifyContent:'center',
        alignItems:'center'
    }
})
