import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import Splash from './Splash'
import {Login} from './Login';
import {Home} from './Home';
import {Detail} from './Detail';
import {Profile} from './Profile';

const Stack = createStackNavigator();

export function Index() {
    return (
        <Stack.Navigator initialRouteName="Splash">
            <Stack.Screen name="Splash" component={Splash} options={{headerShown: false}} />
            <Stack.Screen name="Login" component={Login} options={{headerShown: false}} />
            <Stack.Screen name="Home" component={Home} options={{headerShown: false}} />
            <Stack.Screen name="Detail" component={Detail} options={{headerShown: false}} />
            <Stack.Screen name="Profile" component={Profile} options={{headerShown: false}} />
        </Stack.Navigator> 
    );
}