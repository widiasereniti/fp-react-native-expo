import React from 'react'
import {
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
    ScrollView
} from 'react-native'

export function Profile({navigation}) {
    return (
        <ScrollView>
            <View style={styles.container}>
                <TouchableOpacity onPress={() => navigation.goBack()}>
                    <Image style={styles.backButton} source={require('../assets/images/left-arrow.png')} />
                </TouchableOpacity>

                <View style={styles.containerHeader}>
                    <Text style={styles.about}>Tentang Saya</Text>
                </View>

                <View style={styles.containerProfile}>
                    <Image style={styles.logo} source={require('../assets/images/calum-hood.jpg')}/>
                    <Text style={styles.name}>Calum Hood</Text>
                    <Text style={styles.job}>React Native Developer</Text>
                </View>

                <View style={styles.containerCard}>
                    <Text style={styles.cardTitle}>Portofolio</Text>
                    <View style={styles.containerPortofolioLogo}>
                        <View style={styles.containerCardPortofolioLogo}>
                            <Image style={styles.logoCard} source={require('../assets/images/logo-gitlab.png')} resizeMode='contain'/>
                            <Text style={styles.namePortofolio}>@calumhood</Text>
                        </View>
                        <View style={styles.containerCardPortofolioLogo}>
                            <Image style={styles.logoCard} source={require('../assets/images/logo-github.png')} resizeMode='contain'/>
                            <Text style={styles.namePortofolio}>@calumhood</Text>
                        </View>
                    </View>
                </View>

                <View style={styles.containerCard}>
                    <Text style={styles.cardTitle}>Hubungi Saya</Text>
                    <View style={styles.containerContactLogo}>
                        <View style={styles.containerCardContactLogo}>
                            <Image style={styles.logoCard} source={require('../assets/images/logo-facebook.png')} resizeMode='contain'/>
                            <Text style={styles.nameContact}>@calumhood</Text>
                        </View>
                        <View style={styles.containerCardContactLogo}>
                            <Image style={styles.logoCard} source={require('../assets/images/logo-instagram.png')} resizeMode='contain'/>
                            <Text style={styles.nameContact}>@calumhood</Text>
                        </View>
                        <View style={styles.containerCardContactLogo}>
                            <Image style={styles.logoCard} source={require('../assets/images/logo-twitter.png')} resizeMode='contain'/>
                            <Text style={styles.nameContact}>@calumhood</Text>
                        </View>
                    </View>
                </View>
            </View>
        </ScrollView>
        
    )
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'column',
        justifyContent: 'center',
        backgroundColor: 'white',
        height: '100%'
    },

    backButton: {
        width: 20,
        height: 20,
        marginTop: 50,
        marginLeft: 30,
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
    },

    containerHeader: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },

    btnBack: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'flex-start',
        width: 20,
        height:20,

    },

    logo: {
        width: 300,
        height: 200,
        marginVertical: '3%',
        borderRadius: 7,
    },

    about: {
        fontSize: 27,
        fontWeight: 'bold',
        color: '#003366',
        fontFamily: 'Roboto',
    },

    name: {
        fontSize: 23,
        fontWeight: 'bold',
        color: '#003366',
        fontFamily: 'Roboto',
    },

    job: {
        fontSize: 16,
        fontWeight: 'normal',
        color: '#3EC6FF',
        fontFamily: 'Roboto',
    },

    label: {
        color: '#003366',
        fontSize: 15,
        marginBottom: 10,
    },

    containerProfile: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },

    containerCard: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        height: 'auto',
        backgroundColor: '#EFEFEF',
        borderRadius: 16,
        padding: 10,
        marginVertical: '5%',
        marginHorizontal: '5%'
    },
    
    cardTitle: {
        textAlign: 'left',
        fontWeight: 'bold',
        color: '#003366',
        borderBottomWidth: 1,
        borderBottomColor: '#003366',
        marginBottom: 5,
    },

    containerPortofolioLogo: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: '7%',
        marginVertical: '5%'
    },

    containerContactLogo: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: '7%',
    },

    containerCardPortofolioLogo: {
        alignItems: 'center',
        marginVertical: '2%'
    },

    containerCardContactLogo: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        marginVertical: '2%',
    },

    logoCard: {
        width: 40,
        marginHorizontal: '5%',
        marginVertical: '2%',
    },
});