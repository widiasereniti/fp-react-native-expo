import React from 'react'
import { 
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    TextInput
} from 'react-native'

const Register = () => {
    return (
        <View style={styles.container}>
            <View style={styles.containerWelcome}>
                <Text style={styles.welcomeHeader}>Welcome</Text>
                <Text style={styles.welcomeText}>Sign up for continue</Text>
            </View>
            
            <View style={styles.containerForm}>
                <Text style={styles.label}>Name</Text>
                <TextInput style={styles.textInput} />

                <Text style={styles.label}>Email</Text>
                <TextInput style={styles.textInput} />

                <Text style={styles.label}>Phone Number</Text>
                <TextInput style={styles.textInput} />
                
                <Text style={styles.label}>Password</Text>
                <TextInput style={styles.textInput} />
            </View>

            <View style={styles.containerButton}>
                <TouchableOpacity style={styles.btnLogin}>
                    <Text> Sign In </Text>
                </TouchableOpacity>
                <Text style={styles.haveAccount}>Already have an account? <Text style={styles.linkSignIn}>Sign In</Text></Text>
            </View>
        </View>
    )
}

export default Register

const styles = StyleSheet.create({
    container: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
        height: '100%'
    },

    containerWelcome: {
        width: '85%',
        marginTop: 90,
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
    },

    welcomeHeader: {
        fontWeight: 'bold',
        fontSize: 30,
        color: '#0C0423'
    },

    welcomeText: {
        fontSize: 14,
        color: '#4D4D4D'
    },

    containerForm: {
        marginTop: 11,
        marginHorizontal: '10%',
        paddingHorizontal: '10%',
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
    },

    label: {
        fontSize: 14,
        paddingVertical: '10%'
    },

    textInput: {
        fontSize: 14,
        width: 294,
        height: 1,
        borderBottomWidth: 1,
        borderBottomColor: '#E6EAEE',
    },

    forgotPassword: {
        width: '80%',
        textAlign: 'right',
        color: '#0C0423',
        fontSize: 14,
        marginVertical: '3%',
        marginRight: '5%'
    },

    containerButton: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },

    btnRegister: {
        width: 300,
        backgroundColor: '#F77866',
        borderRadius: 6,
        color: 'white',
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 50,
    },

    haveAccount: {
        fontSize: 14,
        marginVertical: '2%'
    },

    linkSignIn: {
        color: 'red'
    }
})
