import React from 'react'
import { 
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
    TextInput
} from 'react-native'

export function Login({navigation}) {
    return (
        <View style={styles.container}>
            <View style={styles.containerWelcome}>
                <Text style={styles.welcomeHeader}>Welcome Back</Text>
                <Text style={styles.welcomeText}>Sign in for continue</Text>
            </View>
            
            <View style={styles.containerForm}>
                <Text style={styles.label}>Email</Text>
                <TextInput style={styles.textInput} />
                
                <Text style={styles.label}>Password</Text>
                <TextInput style={styles.textInput} secureTextEntry={true}/>
            </View>
            <Text style={styles.forgotPassword}>Forgot Password?</Text>

            <View style={styles.containerButton}>
                <TouchableOpacity style={styles.btnLogin} onPress={() => navigation.navigate('Home')}>
                    <Text> Sign In </Text>
                </TouchableOpacity>
                <Text style={styles.space}>-OR-</Text>
            </View>

            <View style={styles.containerAnotherLogin}>
                <View style={styles.containerSocMed}>
                    <TouchableOpacity style={styles.btnFacebook}>
                        <Image style={styles.socMedLogo} source={require('../assets/images/fb.png')} resizeMode='contain'/>
                        <Text style={styles.socMedName}>Facebook</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.btnGoogle}>
                        <Image style={styles.socMedLogo} source={require('../assets/images/google.png')} resizeMode='contain'/>
                        <Text style={styles.nameSocMed}>Google</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    )
}

export default Login

const styles = StyleSheet.create({
    container: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
        height: '100%'
    },

    containerWelcome: {
        width: '85%',
        marginTop: 90,
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
    },

    welcomeHeader: {
        fontWeight: 'bold',
        fontSize: 30,
        color: '#0C0423'
    },

    welcomeText: {
        fontSize: 12,
        color: '#4D4D4D'
    },

    containerForm: {
        marginTop: 11,
        marginHorizontal: '10%',
        paddingHorizontal: '10%',
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
    },

    label: {
        fontSize: 12,
        paddingVertical: '5%'
    },

    textInput: {
        fontSize: 12,
        width: 294,
        height: 30,
        borderBottomWidth: 1,
        borderBottomColor: '#E6EAEE',
    },

    forgotPassword: {
        width: '80%',
        textAlign: 'right',
        color: '#0C0423',
        fontSize: 12,
        marginVertical: '3%',
        marginRight: '5%'
    },

    containerButton: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },

    btnLogin: {
        width: 300,
        backgroundColor: '#F77866',
        borderRadius: 6,
        color: 'white',
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 30,
    },

    space: {
        marginVertical: 20,
        color: '#4C475A'
    },

    containerSocMed: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: '7%',
    },

    btnFacebook: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        marginVertical: '5%',
        marginRight: '5%',
        borderRadius: 6,
        width: 130,
        height: 40,
        borderWidth: 1,
        borderColor: '#E6EAEE',
        paddingHorizontal: '3%'
    },

    btnGoogle: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        marginVertical: '5%',
        marginLeft: '5%',
        borderRadius: 6,
        width: 130,
        height: 40,
        borderWidth: 1,
        borderColor: '#E6EAEE',
        paddingHorizontal: '3%'
    },

    socMedLogo: {
        alignItems: 'center',
    },

    socMedName:{
        fontSize: 14,
        color:'#4D4D4D'
    }
})
