import React from 'react'
import { 
    View,
    Text,
    Image,
    ScrollView,
    TextInput,
    StyleSheet,
    TouchableOpacity
} from 'react-native'

export function Home({navigation}) {
    return (
        <ScrollView>
            <View style={styles.container} >
                <View style={styles.containerSearch}>
                    <View style={styles.containerSearchBar}>
                        <View style={styles.searchBar}>
                            <TextInput placeholder='Search Product' style={styles.textInput} />
                        </View>
                        <TouchableOpacity onPress={() => navigation.navigate('Profile')}>
                            <Image style={styles.btnProfile} source={require('../assets/images/user.png')} />
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={styles.containerSlider}>
                    <Image style={styles.slider} source={require('../assets/images/Slider.png')}/>
                </View>

                <View style={styles.containerCategory}>
                    <View style={styles.Category}>
                        <Image style={styles.iconCategory} source={require('../assets/images/Cat_Man.png')}/>
                        <Text style={styles.textInput} >Man</Text>
                    </View>
                    <View style={styles.Category}>
                        <Image style={styles.iconCategory} source={require('../assets/images/Cat_Women.png')}/>
                        <Text style={styles.textInput} >Women</Text>
                    </View>
                    <View style={styles.Category}>
                        <Image style={styles.iconCategory} source={require('../assets/images/Cat_Kids.png')}/>
                        <Text style={styles.textInput} >Kids</Text>
                    </View>
                    <View style={styles.Category}>
                        <Image style={styles.iconCategoryHome} source={require('../assets/images/Cat_Home.png')}/>
                        <Text style={styles.textInput} >Home</Text>
                    </View>
                    <View style={styles.Category}>
                        <Image style={styles.iconCategory} source={require('../assets/images/Cat_Back.png')}/>
                        <Text style={styles.textInput} >More</Text>
                    </View>
                    
                </View>

                <Text style={styles.headerFlash} >Flash Sell</Text>
                <View style={styles.containerCategory}>
                    <TouchableOpacity style={styles.Category} onPress={() => navigation.navigate('Detail')}>
                        <Image style={styles.flash} source={require('../assets/images/Rectangle-4.png')}/>
                        <Text style={styles.product} >Tiare Handwash</Text>
                        <Text style={styles.price} >$12.23</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.Category} onPress={() => navigation.navigate('Detail')}>
                        <Image style={styles.flash} source={require('../assets/images/Rectangle-3.png')}/>
                        <Text style={styles.product} >JBL Speaker</Text>
                        <Text style={styles.price} >$12.23</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.Category} onPress={() => navigation.navigate('Detail')}>
                        <Image style={styles.flash} source={require('../assets/images/Rectangle-2.png')}/>
                        <Text style={styles.product} >Google Home</Text>
                        <Text style={styles.price} >$80.00</Text>
                    </TouchableOpacity>
                </View>

                <Text style={styles.headerNewProduct} >New Product</Text>
                <View style={styles.containerProduct}>
                    <View style={styles.Product}>
                        <Image style={styles.imageProduct} source={require('../assets/images/Rectangle-1.png')}/>
                        <Text style={styles.product} >Nike Man Shoes</Text>
                        <Text style={styles.price} >$50.53</Text>
                    </View>
                    <View style={styles.Product}>
                        <Image style={styles.imageProduct} source={require('../assets/images/Rectangle.png')}/>
                        <Text style={styles.product} >IKEA White Chair</Text>
                        <Text style={styles.price} >$30.50</Text>
                    </View>
                </View>
                

            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'column',
        justifyContent: 'center',
        backgroundColor: 'white',
    },

    containerSearchBar: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: 50,
        paddingHorizontal: '5%',
    },

    searchBar: {
        width: 320,
        height: 40,
        borderWidth: 0.5,
        borderStyle: 'solid',
        borderColor: '#727C8E',
        borderRadius: 11,
        justifyContent: 'center',
        paddingHorizontal: '5%'
    },

    containerSlider: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },

    btnProfile: {
        marginLeft: 7,
        width: 40,
        height:40,
        borderRadius: 50 / 2,
    },

    slider: {
        width: 350,
        height: 200,
        marginVertical: '5%',
        borderRadius: 7,
    },

    containerCategory: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: '5%',
    },

    Category: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },

    iconCategory: {
        maxWidth: 65,
        maxHeight: 65
    },

    iconCategoryHome: {
        maxWidth: 46,
        maxHeight: 46,
        marginBottom: 12
    },

    textInput: {
        fontSize: 12,
    },

    headerFlash: {
        marginVertical: '5%',
        marginHorizontal: '5%',
        fontSize: 20,
        fontWeight: 'bold'
    },

    flash: {
        width: 100,
        height: 100,
        borderRadius: 5,
        marginVertical: 5
    },

    price: {
        fontWeight: 'bold',
    },

    //Product
    containerProduct: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: '5%',
    },

    headerNewProduct: {
        marginVertical: '5%',
        marginHorizontal: '5%',
        fontSize: 20,
        fontWeight: 'bold'
    },

    imageProduct: {
        width: 160,
        height: 160,
        borderRadius: 5,
        marginVertical: 5
    },

    price: {
        fontWeight: 'bold',
        marginBottom: 20
    }
});
